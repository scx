#include <iostream>
using namespace std;

template<typename arg_t>
void fn(arg_t arg)
{
    cout << &arg << endl;
}

int main()
{
    int a;
    cout << &a << endl;
    fn<int>(a);
    fn<int&>(a);
    return 0;
}
