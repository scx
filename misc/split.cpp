#include <iostream>
#include <vector>
#include <string>
#include <sstream>
#include <iterator>
#include <algorithm>
using namespace std;

template<typename item_t, typename iter_t>
void CoutEachItem(iter_t begin, iter_t end, const string& space = "\n")
{
    std::copy(begin, end, std::ostream_iterator<item_t>(cout, space.c_str()));
}

int main()
{
    vector<string> tokens;
    string sentence = "something  a like you? ...";
    istringstream iss(sentence);

    copy(istream_iterator<string>(iss),
	 istream_iterator<string>(),
	 back_inserter<vector<string> >(tokens));
    
    CoutEachItem<string>(tokens.begin(), tokens.end(), "|");
    /*
    copy(istream_iterator<string>(iss),
	 istream_iterator<string>(),
	 ostream_iterator<string>(cout, "\n"));
    */

    return 0;
}
