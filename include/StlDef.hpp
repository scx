#ifndef StlDef_h
#define StlDef_h

#include <vector>
#include <string>
using namespace std;

namespace scx
{

typedef vector<short> ShortVector;
typedef vector<int> IntVector;
typedef vector<size_t> UIntVector;
typedef vector<double> DoubleVector;
typedef vector<String> StringVector;
    
};

#endif
