#ifndef SCX_STACK_HPP
#define SCX_STACK_HPP

namespace scx {

template<typename T, int MAX>
class StaticStack
{
public:
    StaticStack(): mIndex(0) { }

    void Push(const T& val)
    {
        mValues[mIndex++] = val;
    }

    void Pop(T& val)
    {
        val = mValues[--mIndex];
    }

    T Pop()
    {
        T val;
        Pop(val);
        return val;
    }

    void Clear()
    {
        mIndex = 0;
    }

    void Top(T& val) const
    {
        val = mValues[mIndex-1];
    }

    T Top() const
    {
        T val;
        Top(val);
        return val;
    }

    void Bottom(T& val) const
    {
        val = mValues[0];
    }

    T Bottom() const
    {
        T val;
        Top(val);
        return val;
    }

    void At(T& val, int pos) const
    {
        val = mValues[pos];
    }

    T At(int pos) const
    {
        T val;
        At(val, pos);
        return val;
    }

    int Max() const
    {
        return MAX;
    }

    int Free() const
    {
        return MAX - mIndex;
    }

    int Size() const
    {
        return mIndex;
    }

    bool Empty() const
    {
        return mIndex == 0;
    }

    bool Full() const
    {
        return mIndex == MAX;
    }

private:
    T mValues[MAX];
    int mIndex;
};

}

#endif
