#ifndef SCX_COLSTRSTRING_H
#define SCX_COLSTRSTRING_H

#include <string>
using namespace std;

#define SCX_COLSTR_RED0		"\e[0;31m"
#define SCX_COLSTR_RED1		"\e[1;31m"
#define SCX_COLSTR_BLUE0	"\e[0;34m"
#define SCX_COLSTR_BLUE1	"\e[1;34m"
#define SCX_COLSTR_CYAN0	"\e[0;36m"
#define SCX_COLSTR_CYAN1	"\e[1;36m"
#define SCX_COLSTR_GREEN0	"\e[0;32m"
#define SCX_COLSTR_GREEN1	"\e[1;32m"
#define SCX_COLSTR_YELLOW0	"\e[0;33m"
#define SCX_COLSTR_YELLOW1	"\e[1;33m"
#define SCX_COLSTR_NC		"\e[0m"

namespace scx {

static inline string RedStr(const string& str, bool front = true)
{
    return (front ? SCX_COLSTR_RED1 : SCX_COLSTR_RED0) + 
	str + SCX_COLSTR_NC;
}

static inline string BlueStr(const string& str, bool front = true)
{
    return (front ? SCX_COLSTR_BLUE1 : SCX_COLSTR_BLUE0) +
	str + SCX_COLSTR_NC;
}

static inline string CyanStr(const string& str, bool front = true)
{
    return (front ? SCX_COLSTR_CYAN1 : SCX_COLSTR_CYAN0) +
	str + SCX_COLSTR_NC;
}

static inline string GreenStr(const string& str, bool front = true)
{
    return (front ? SCX_COLSTR_GREEN1 : SCX_COLSTR_GREEN0) +
	str + SCX_COLSTR_NC;
}

static inline string YellowStr(const string& str, bool front = true)
{
    return (front ? SCX_COLSTR_YELLOW1 : SCX_COLSTR_YELLOW0) +
	str + SCX_COLSTR_NC;
}
};

#endif
