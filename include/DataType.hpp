#ifndef SCX_DATATYPE_HPP
#define SCX_DATATYPE_HPP

namespace scx {

    template <typename T>
    struct Value
    {
	T val;
	T max;
	T min;

	Value()
	{
	}

	Value(T a, T b, T c):
	    val(a),
	    max(b),
	    min(c)
	{
	}

	Value(T a):
	    val(a)
	{
	}

	Value(T a, T b):
	    max(a),
	    min(b)
	{
	}

	Value(const Value& val):
	    val(val.val),
	    max(val.max),
	    min(val.min)
	{
	}
    };
};

#endif
