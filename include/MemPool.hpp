#ifndef SCX_MEM_POOL
#define SCX_MEM_POOL

#ifdef SCX_LOG
#include <iostream>
#endif
#include <cstdlib>
#include "Alias.hpp"

namespace scx {

/**
 * The BLOCK_STEP at least should be sizeof(void*).
 * It means on amd64 the min step is 8, while on i386 the min step is 4.
 */
template<int BLOCK_STEP = 8, int BLOCK_COUNT = 16>
class MemPool {
public:
    explicit MemPool(int secCount = 1024):
        mSecCount(secCount),
        mMemList(NULL),
        mPoolSize(0)
    {
        Init();
    }

    ~MemPool()
    {
        Free();
    }

private:
    void Init()
    {
        for (int i = 0; i < BLOCK_COUNT; ++i) {
            mFreeBlocks[i] = NULL;
        }
    }
    
public:
    /**
     * If the size is large than the max block size,<br/>
     * we'll directly use malloc(). <br/>
     * So the potential problem is: <br/>
     * you should always remember to call Reuse() to free such memory, <br/>
     * as such memory won't be record in mMemList(so, it won't be freed by Free()/dtor).
     */
    void* Alloc(size_t size)
    {
        size = SCX_ALIGN(size, BLOCK_STEP);
        int index = (size-1)/BLOCK_STEP;
        if (index < BLOCK_COUNT) {
            if (mFreeBlocks[index] == NULL) {
#ifdef SCX_LOG
                std::cout << "." << std::flush;
#endif
                size_t total = sizeof(Mem) + mSecCount*size;
                Mem* mem = (Mem*)malloc(total);
                mem->buf = (char*)mem + sizeof(Mem);
                mem->next = mMemList;
                mMemList = mem;
                mPoolSize += total;
                Sec* sec = (Sec*)mMemList->buf;
                mFreeBlocks[index] = sec;
                for (int i = 0; i < mSecCount-1; ++i) {
                    sec->next = (Sec*)((char*)sec+size);
                    sec = sec->next;
                }
                sec->next = NULL;
            }
            Sec* sec = mFreeBlocks[index];
            mFreeBlocks[index] = sec->next;
            return sec;
        } else {
#ifdef SCX_LOG
            std::cout << "!" << size << std::flush;
#endif      
            mPoolSize += size;
            return malloc(size);
        }
    }

    void Reuse(void* p, int size)
    {
        size = SCX_ALIGN(size, BLOCK_STEP);
        int index = (size-1)/BLOCK_STEP;
        if (index < BLOCK_COUNT) {
            Sec* sec = (Sec*)p;
            sec->next = mFreeBlocks[index];
            mFreeBlocks[index] = sec;
        } else {
            free(p);
        }
    }

    /**
     * Free all the memory we've allocated.
     */
    void Free()
    {
        for (Mem* mem = mMemList; mMemList != NULL; mem = mMemList) {
            mMemList = mem->next;
            free(mem);
        }
        mPoolSize = 0;
        Init();
    }

    /**
     * The pool size means how many bytes we've allocated,<br/>
     * not the available size for Alloc() before next allocation. 
     */
    size_t Size() const
    {
        return mPoolSize;
    }

    void SetSecCount(int secCount)
    {
        mSecCount = secCount;
    }

    int GetSecCount() const
    {
        return mSecCount;
    }

    void PreAlloc(size_t size)
    {

    }

private:
    struct Sec
    {
        Sec* next;
    };

    struct Mem
    {
        void* buf;
        Mem* next;
    };

    int mSecCount;
    Mem* mMemList;
    size_t mPoolSize;
    Sec* mFreeBlocks[BLOCK_COUNT];
};

}
#endif
