#ifndef SCX_SQLITEKV_HPP
#define SCX_SQLITEKV_HPP

#include <sqlite3.h>
#include <zlib.h>
#include <string>
#include <cstdio>
#include <cstring>

namespace scx {

class SqliteKV
{
public:
    SqliteKV(const std::string& dbName = ""):
        BEGIN_TRANSACTION("BEGIN;"),
        COMMIT_TRANSACTION("COMMIT;"),
        CREATE_TABLE("create table if not exists %s(KEY text primary key, VALUE blob);"),
        DROP_TABLE("drop table if exists %s;"),
        INSERT_PAIR("insert into %s values('%s', ?);"),
        SELECT_VALUE("select VALUE from %s where KEY='%s';"),
        UPDATE_VALUE("update %s set VALUE=? where KEY='%s';"),
        DROP_PAIR("drop from %s where KEY='%s';"),
        COUNT_PAIRS("select count(key) from %s;"),
        COUNT_RESULTS("select count(key) from %s where KEY='%s';"),
        mDbName(dbName),
        mTableName(""),
        mDb(NULL),
        mSqlBufLen(200),
        mSqlBuf(new char[200])
    {
        if (!mDbName.empty()) 
            OpenDb(mDbName);
    }

    ~SqliteKV()
    {
        if (!mDbName.empty())
            CloseDb();
        if (mSqlBuf != NULL)
            delete mSqlBuf;
    }

    void OpenDb(const std::string& dbName)
    {
        mDbName = dbName;
        sqlite3_open(mDbName.c_str(), &mDb);

        sqlite3_exec(mDb, "PRAGMA journal_mode=off;", NULL, 0, 0);
        sqlite3_exec(mDb, "PRAGMA synchronous=off;", NULL, 0, 0);
        sqlite3_exec(mDb, "PRAGMA cache_size=8000;", NULL, 0, 0);
        //sqlite3_exec(mDb, "PRAGMA page_size=2048;", NULL, 0, 0);
        //sqlite3_exec(mDb, "PRAGMA page_count=200;", NULL, 0, 0);
    }

    void CloseDb()
    {
        mDbName.clear();
        sqlite3_finalize(mStmt);
        sqlite3_close(mDb);
    }

    // switch table (if the targe table doesn't exist, I'll create it before switch)
    void SwitchTable(const std::string& tableName)
    {
        mTableName = tableName;
        CheckSqlBuf(CREATE_TABLE.size() + mTableName.size());
        snprintf(mSqlBuf, mSqlBufLen, CREATE_TABLE.c_str(), tableName.c_str());
        sqlite3_exec(mDb, mSqlBuf, NULL, 0, 0);
    }

    // drop the current table
    void DropTable()
    {
        CheckSqlBuf(DROP_TABLE.size() + mTableName.size());
        snprintf(mSqlBuf, mSqlBufLen, DROP_TABLE.c_str(), mTableName.c_str());
        sqlite3_exec(mDb, mSqlBuf, NULL, 0, 0);
    }

    void Begin()
    {
        sqlite3_exec(mDb, BEGIN_TRANSACTION.c_str(), NULL, 0, 0);
    }

    void Commit()
    {
        sqlite3_exec(mDb, COMMIT_TRANSACTION.c_str(), NULL, 0, 0);
    }

    /* put family */

    // put fixed-size data, by sizeof(T)
    template<typename T>
    bool PutData(const std::string& key, const T& data)
    {
        return PutRaw(key, (char*)&data, sizeof(T));
    }

    // put string data
    bool PutString(const std::string& key, const std::string& text)
    {
        return PutRaw(key, text.c_str(), text.size());
    }

    // put raw data
    bool PutRaw(const std::string& key, const char* raw, int size)
    {
        CheckSqlBuf(INSERT_PAIR.size() + mTableName.size() + key.size());
        snprintf(mSqlBuf, mSqlBufLen, INSERT_PAIR.c_str(), mTableName.c_str(), key.c_str());
        sqlite3_prepare_v2(mDb, mSqlBuf, -1, &mStmt, 0);
        sqlite3_bind_blob(mStmt, 1, raw, size, SQLITE_STATIC);
        sqlite3_step(mStmt);
        sqlite3_finalize(mStmt);
        return sqlite3_changes(mDb) == 0;
    }

    /* get family */

    // get type data
    template<class T>
    bool GetData(const std::string& key, T& data)
    {
        const void* blob;
        int size;
        if (GetBlob(key, blob, size))
        {
            memcpy(&data, blob, size);
            return true;
        }
        else
        {
            return false;
        }
    }

    // get string data
    bool GetString(const std::string& key, std::string& text)
    {
        const void* blob;
        int size;
        if (GetBlob(key, blob, size))
        {
            text.assign((const char*)blob, size);
            return true;
        }
        else
        {
            return false;
        }
    }

    // get raw data, false if null or not found 
    bool GetRaw(const std::string& key, char*& raw, int& size)
    {
        const void* blob;
        if (GetBlob(key, blob, size))
        {
            raw = new char[size];
            memcpy(raw, blob, size);
            return true;
        }
        else
        {
            return false;
        }
    }

    /* update family */

    template<typename T>
    bool UpdateData(const std::string& key, const T& data)
    {
        return UpdateRaw(key, (char*)&data, sizeof(T));
    }

    bool UpdateString(const std::string& key, const std::string& text) 
    {
        return UpdateRaw(key, text.c_str(), text.size());
    }

    bool UpdateRaw(const std::string& key, const char* raw, int size)
    {
        //CheckSqlBuf(UPDATE_VALUE.size() + mTableName.size() + key.size());
        snprintf(mSqlBuf, mSqlBufLen, UPDATE_VALUE.c_str(), mTableName.c_str(), key.c_str());
        sqlite3_prepare_v2(mDb, mSqlBuf, -1, &mStmt, 0);
        sqlite3_bind_blob(mStmt, 1, raw, size, SQLITE_STATIC);
        sqlite3_step(mStmt);
        return true;
    }

    // delete pair by key
    bool Delete(const std::string& key)
    {
        CheckSqlBuf(DROP_TABLE.size() + mTableName.size());
        snprintf(mSqlBuf, mSqlBufLen, DROP_TABLE.c_str(), mTableName.c_str());
        sqlite3_exec(mDb, mSqlBuf, NULL, 0, 0);
        return true;
    }

    // number of pairs in table
    int Count()
    {
        CheckSqlBuf(COUNT_PAIRS.size() + mTableName.size());
        snprintf(mSqlBuf, mSqlBufLen, COUNT_PAIRS.c_str(), mTableName.c_str());
        sqlite3_prepare_v2(mDb, mSqlBuf, -1, &mStmt, 0);
        sqlite3_step(mStmt);
        return sqlite3_column_int(mStmt, 0);
    }

private:
    void CheckSqlBuf(size_t len)
    {
    /*
    if (mSqlBufLen < len)
    {
        delete mSqlBuf;
        mSqlBuf = new char[len];
        mSqlBufLen = len;
    }
    */
    }

    bool GetBlob(const std::string& key, const void*& blob, int& size)
    {
        CheckSqlBuf(SELECT_VALUE.size() + mTableName.size() + key.size());
        snprintf(mSqlBuf, mSqlBufLen, SELECT_VALUE.c_str(), mTableName.c_str(), key.c_str());
        sqlite3_prepare_v2(mDb, mSqlBuf, -1, &mStmt, 0);
        sqlite3_step(mStmt);
        blob = sqlite3_column_blob(mStmt, 0);
        size = sqlite3_column_bytes(mStmt, 0);
        return (blob != NULL);
    }

private:
    static int OnRow(void*, int argc, char** argv, char** colName)
    {
        return 0;
    }

private:
    const std::string BEGIN_TRANSACTION;
    const std::string COMMIT_TRANSACTION;
    const std::string CREATE_TABLE;
    const std::string DROP_TABLE;
    const std::string INSERT_PAIR;
    const std::string SELECT_VALUE;
    const std::string UPDATE_VALUE;
    const std::string DROP_PAIR;
    const std::string COUNT_PAIRS;
    const std::string COUNT_RESULTS;
  
private:
    std::string mDbName;
    std::string mTableName;
    sqlite3* mDb;
    sqlite3_stmt* mStmt;
    size_t mSqlBufLen;
    char* mSqlBuf;
};

}

#endif
