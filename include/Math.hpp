#ifndef SCX_ROUNDOFF_HPP
#define SCX_ROUNDOFF_HPP

#include <cmath>
#include <sstream>
using namespace std;

namespace scx {

static inline double RoundOff(double num, int n)
{
    int scale = pow((double)10, n);
    num += 0.5 * pow((double)0.1, n);
    num *= scale;
    num = (int)num;
    num /= scale;
    return num;
}

template<class T>
static inline bool IsOdd(T n)
{
    return (n & 1);
}

template<class T>
static inline bool IsEven(T n)
{
    return !(n & 1);
}

// only for unsigned
template<class T>
static inline bool Is2Exp(T n)
{
    return !(n & (n-1));
}

template<typename T>
static inline bool Not2Exp(T n)
{
    return (n & (n-1));
}

}

#endif
