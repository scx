#ifndef SCX_HTML_HPP
#define SCX_HTML_HPP

#include <vector>
#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
using namespace std;

/*
 * only for regular table.
 * means each row has the same cols.
*/

namespace scx {

struct HtmlTableInfo
{
    size_t begin;
    size_t end;
    size_t rows;
    size_t cols;

    HtmlTableInfo():
    begin(0),
    end(0),
    rows(0),
    cols(0)
    {
    }
};

typedef vector<HtmlTableInfo> HtmlTablesInfo;
typedef vector<vector<string> > HtmlTableData;

class HtmlTable
{
public:
    HtmlTable():
    TABLE_BEGIN("<table"),
    TABLE_END("</table>"),
    ROW_BEGIN("<tr"),
    ROW_END("</tr>"),
    DATA_BEGIN("<td"),
    DATA_END("</td>")
    {
    }

    ~HtmlTable()
    {
    }

    void LoadBuffer(const char* buffer, size_t size)
    {
        mBuffer.assign(buffer, size);
    }

    void LoadFile(const string& path)
    {
        fstream file;
        stringstream stream;

        file.open(path.c_str());
        stream << file.rdbuf();
        file.close();

        mBuffer = stream.str();
    }

    HtmlTablesInfo CheckInfo()
    {
        size_t tbBeg = 0;
        size_t tbEnd = 0;

        mTablesInfo.clear();

        while (true) {
            tbBeg = mBuffer.find(TABLE_BEGIN, tbEnd);
            if (tbBeg == string::npos)
                break;
            tbEnd = mBuffer.find(TABLE_END, tbBeg);
            if (tbEnd == string::npos)
                break;

            size_t trBeg = 0;
            size_t trEnd = tbBeg;
            size_t rows = 0;
            size_t cols = 0;
            while (trEnd < tbEnd) {
                trBeg = mBuffer.find(ROW_BEGIN, trEnd);
                if (trBeg == string::npos || trBeg >= tbEnd)
                    break;
                trEnd = mBuffer.find(ROW_END, trBeg);
                if (trEnd == string::npos || trBeg >= tbEnd)
                    break;
                ++rows;

                if (cols == 0) {
                    size_t tdBeg = 0;
                    size_t tdEnd = trBeg;
                    while (tdEnd < trEnd) {
                        tdBeg = mBuffer.find(DATA_BEGIN, tdEnd);
                        if (tdBeg == string::npos || tdBeg >= trEnd)
                            break;
                        tdEnd = mBuffer.find(DATA_END, tdBeg);
                        if (tdEnd == string::npos || tdEnd >= trEnd)
                            break;
                        ++cols;
                    }
                }
            }

            HtmlTableInfo info;
            info.begin = tbBeg;
            info.end = tbEnd;
            info.rows = rows;
            info.cols = cols;
            mTablesInfo.push_back(info);
        }

        return mTablesInfo;
    }

    void ReadAll(vector<HtmlTableData>& allData)
    {
        size_t tables = mTablesInfo.size();
        allData.resize(tables);
        for (size_t i = 0; i < tables; ++i) {
            const size_t& tbBeg = mTablesInfo[i].begin;
            const size_t& tbEnd = mTablesInfo[i].end;
            const size_t& rows = mTablesInfo[i].rows;
            const size_t& cols = mTablesInfo[i].cols;
            HtmlTableData& data = allData[i];

            data.resize(rows);

            size_t trBeg = 0;
            size_t trEnd = tbBeg;
            for (size_t r = 0; r < rows; ++r) {
                data[r].resize(cols);

                trBeg = mBuffer.find(ROW_BEGIN, trEnd);
                if (trBeg == string::npos || trBeg >= tbEnd)
                    break;
                trEnd = mBuffer.find(ROW_END, trBeg);
                if (trEnd == string::npos || trBeg >= tbEnd)
                    break;

                size_t tdBeg = 0;
                size_t tdEnd = trBeg;
                for (size_t c = 0; c < cols; ++c) {
                    tdBeg = mBuffer.find(DATA_BEGIN, tdEnd);
                    tdEnd = mBuffer.find(DATA_END, tdBeg);
                    size_t beg = tdBeg;
                    for (; beg < tdEnd; ++beg) {
                        beg = mBuffer.find(">", beg);
                        if (beg == string::npos || beg >= tdEnd) {
                            beg = string::npos;
                            break;
                        }
                        if (mBuffer[beg+1] != '<') {
                            ++beg;
                            break;
                        }
                    }
                    if (beg != string::npos) {
                        size_t end = mBuffer.find("<", beg);
                        if (end != string::npos && end <= tdEnd) {
                            data[r][c] = mBuffer.substr(beg, end-beg);
                        }
                    }
                }
            }
        }
    }

    bool Read(size_t index, HtmlTableData& data)
    {
        return true;
    }

private:
    string mBuffer;
    HtmlTablesInfo mTablesInfo;

    const string TABLE_BEGIN;
    const string TABLE_END;
    const string ROW_BEGIN;
    const string ROW_END;
    const string DATA_BEGIN;
    const string DATA_END;
};

};

#endif
