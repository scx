#ifndef SCX_QUEUE_HPP
#define SCX_QUEUE_HPP

#include <cassert>
#include "Alias.hpp"
#include "MemPool.hpp"

namespace scx {

template<typename T, short BLOCK_VALUE_COUNT=8>
class Queue
{
struct Block;

public:
    Queue():
        mFirstBlock(NULL),
        mLastBlock(NULL),
        mSecondLastBlock(NULL),
        mBlockCount(0),
        mValueCount(0),
        mCacheBlock(NULL),
        mCacheCount(0),
        mCacheTotal(0),
        mPool(1024*10)
    {

    }

    ~Queue()
    {
        Clear();
    }

    void Clear()
    {
        FreeBlocksFrom(mFirstBlock);
        FreeBlocksFrom(mCacheBlock);

        mFirstBlock = NULL;
        mLastBlock = NULL;
        mSecondLastBlock = NULL;
        mBlockCount = 0;
        mValueCount = 0;
        mCacheBlock = NULL;
        mCacheCount = 0;
        mCacheTotal = 0;
    }

    void PopFront(T& val)
    {
        val = mFirstBlock->values[mFirstBlock->usedStart++];
        if (mFirstBlock->usedStart >= mFirstBlock->freeStart) {
            Block* block = mFirstBlock;
            mFirstBlock = block->next;
            RecycleBlock(block);
            --mBlockCount;
        }
        --mValueCount;
    }

    T PopFront()
    {
        T value;
        PopFront(value);
        return value;
    }

    void PopBack(T& val)
    {
        --mValueCount;
    }

    T PopBack()
    {
        T value;
        PopBack(value);
        return value;
    }

    void PushBack(const T& val)
    {
        if (mBlockCount <= 0) {
            mFirstBlock = mLastBlock = GenerateBlock();
            ++mBlockCount;
        } else if (mLastBlock->freeStart >= BLOCK_VALUE_COUNT) {
            mLastBlock->next = GenerateBlock();
            ++mBlockCount;
            mLastBlock = mLastBlock->next;
        }
        mLastBlock->values[mLastBlock->freeStart++] = val;
        ++mValueCount;
    }

    void PushFront(const T& val)
    {
        ++mValueCount;
    }

    void SetCacheSize(int count)
    {
        mCacheTotal = count;
    }

    int GetCacheSize() const
    {
        return mCacheTotal;
    }

    void FreeCache()
    {
        mCacheCount = 0;
        mCacheTotal = 0;
    }

private:
    Block* GenerateBlock()
    {
        Block* block = NULL;
        if (mCacheCount <= 0) {
            block = (Block*) mPool.Alloc(sizeof(Block));
            new(block) Block();
            block->next = NULL;
            block->freeStart = 0;
            block->usedStart = 0;
            /*
               block = (Block*)new char[SCX_QALIGN(sizeof(Block), 16)];
               new(block) Block();
               block->next = NULL;
               block->freeStart = 0;
               block->usedStart = 0;
               */
        } else {
            block = mCacheBlock;
            mCacheBlock = block->next;
            --mCacheCount;
            block->next = NULL;
            block->freeStart = 0;
            block->usedStart = 0;
        }
        return block;
    }

    void RecycleBlock(Block* block)
    {
        if (mCacheCount < mCacheTotal) {
            block->next = mCacheBlock;
            mCacheBlock = block;
            ++mCacheCount;
        } else {
            block->~Block();
            mPool.Reuse(block, sizeof(Block));
            //delete block;
        }
    }

    void FreeBlocksFrom(Block* first)
    {
        while (first != NULL) {
            Block* block = first;
            first = first->next;
            block->~Block();
            mPool.Reuse(block, sizeof(Block));
            //delete block;
        }
    }

private:
    struct Block
    {
        Block* next;
        short freeStart;
        short usedStart;
        T values[BLOCK_VALUE_COUNT];

        Block(): 
            next(NULL), 
            freeStart(0), usedStart(0)
        {
        }

        ~Block()
        {
        }
    };

    Block* mFirstBlock;
    Block* mLastBlock;
    Block* mSecondLastBlock;
    int mBlockCount;
    int mValueCount;

    Block* mCacheBlock;
    int mCacheCount;
    int mCacheTotal;

    MemPool<sizeof(Block), 1> mPool;
};

}
#endif
