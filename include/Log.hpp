#ifndef SCX_LOG_HPP
#define SCX_LOG_HPP

#include <iostream>
#include <fstream>
#include <sstream>
#include <string>
#include "Mutex.hpp"
#include "ColStr.hpp"

namespace scx
{
class Log
{
public:
    Log()
    {
        EnableStdOut(true);
        EnableFileOut(false);
    }

    ~Log()
    {
        Close();
    }

    bool Open(const std::string& file = "log.txt")
    {
        m_FileStream.open(file.c_str(), std::ios::out);
        if (m_FileStream.is_open())
            return true;
        else
            return false;
    }

    void Close()
    {
        if (m_FileStream.is_open())
            m_FileStream.close();
    }

    stringstream& Info()
    {
        return m_StrStream;
    }

    stringstream& Warning()
    {
        return m_StrStream;
    }

    stringstream& Error()
    {
        return m_StrStream;
    }

    template <typename T>
    ostream& operator<<(T& val)
    {
        m_FileStream << val;
        cout << val;
        return m_FileStream;
    }

    ostream& operator<<(ostream& (*fn)(ostream&))
    {
        m_FileStream << fn;
        cout << fn;
        return m_FileStream;
    }

    ostream& operator<<(ios& (*fn)(ios&))
    {
        return m_FileStream;
    }

private:
    Mutex m_Mutex;
    bool m_EnableStdOut;
    bool m_EnableFileOut;
    std::ofstream m_FileStream;
    std::stringstream m_StrStream;
};

class ColorStream
{
public:
    template <typename T>
    ostream& operator<<(T&)
    {

    }

private:

}
}

#endif
