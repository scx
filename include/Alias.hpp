#ifndef SCX_ALIAS_HPP
#define SCX_ALIAS_HPP

// align
#define SCX_ALIGN(d, a)	    ( ( (int)(( (d)-1 )/(a)) + 1 ) * (a) )
#define SCX_QALIGN(d, a)    ( ( (d) + ((a) - 1) ) & ~((a) - 1) )

// bit
#define SCX_BIT(n)	( 1 << (n) )

// len
#define SCX_ARRAY_LEN(a)	(sizeof(a)/sizeof(a[0]))

// for
#define SCX_FOR(index, begin, _end)			\
    for (size_t index = begin, end = _end;		\
	 index < end;					\
	 ++index)					    

// for stl vector etc.
#define foreach_linear(index, container)		\
    for (size_t index = 0;				\
	 index < (container).size();			\
	 ++index)

#define forrange_linear(index, begin, end)		\
    for (size_t index = begin;				\
	 index < end;					\
	 ++index)

// for stl map etc.
#define foreach_iterator(iter_t, iter, container)	\
    for (iter_t iter = container.begin();		\
	 iter < container.end();			\
	 ++iter)

#define forrange_iterator(iter_t, iter, begin, end)	\
    for (iter_t iter = begin;				\
	 iter != end;					\
	 ++iter)
#endif
