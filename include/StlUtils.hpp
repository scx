#ifndef SCX_STL_UTILS_HPP
#define SCX_STL_UTILS_HPP

#include <algorithm>
using namespace std;

namespace scx {

#define SCX_FNOBJ(id, args, list, init, opts)	\
struct FnObj##id {				\
    args;					\
\
    FnObj##id()list { init; }			\
    ~FnObj##id() { cout << "!"; }			\
\
    template<typename T>			\
    void operator()(T& val) { opts; }	\
}

template<typename C>
static inline typename C::value_type LinearSum(const C& container, size_t begin, size_t end) {
    typename C::value_type sum = 0;
    for (size_t i = 0; i < end; ++i) {
	sum += container[i];
    }
    return sum;
}

template<typename C>
static inline typename C::value_type LinearSum(const C& container, size_t begin) {
    return LinearSum(container, begin, container.size());
}

template<typename C>
static inline typename C::value_type LinearSum(const C& container) {
    return LinearSum(container, 0, container.size());
}

template<typename C>
static inline C LinearSum(const C array[], size_t begin, size_t end) {
    C sum = 0;
    for (size_t i = 0; i < end; ++i) {
	sum += array[i];
    }
    return sum;
}

template<typename C>
static inline C LinearSum(C array[], size_t begin) {
    return LinearSum(array, begin, sizeof((C[])array)/sizeof(C));
}

}

#endif
