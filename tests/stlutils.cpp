#include <iostream>
#include <vector>
#include <deque>
#include <map>
#include "scx/All.hpp"
using namespace std;
using namespace scx;

int main() {
    {
	const int count = 100;
	int intarray[count];
	vector<int> array;
	array.resize(count);

	SCX_FOR(i, 0, count) {
	    intarray[i] = i;
	}

	SCX_FOR(i, 0, count) {
	    array[i] = i;
	}

	/*
	SCX_FNOBJ(sum0, 
		int sum, 
		:sum(0),
		,
		val += 1) sum0obj;
	for_each(array.begin(), array.end(), sum0obj);
	cout << "sum:" << sum0obj.sum << endl;

	SCX_FNOBJ(show, 
		, 
		,
		,
		cout << val << ", ") showobj;
	for_each(array.begin(), array.end(), showobj);
	cout << endl;
	*/
	cout << LinearSum(array) << endl;
	cout << LinearSum(intarray, 0, SCX_ARRAY_LEN(intarray)) << endl;
    }

    return 0;
}
