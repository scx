#include <iostream>
#include "scx/DataType.hpp"
using namespace std;

int main(int argc, char** argv)
{
    scx::Value<int> a;
    a.val = 10;
    a.max = 20;
    a.min = 0;

    scx::Value<int> b(a);
    cout << b.val << endl;
    cout << b.max << endl;
    cout << b.min << endl;
}
