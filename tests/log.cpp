#include "scx/Log.hpp"
using namespace scx;

int main()
{
    Log log;
    log.EnableFileOut();
    log << "hello world" << endl;
    log.EnableFileOut(false);
    return 0;
}
