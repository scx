#include <unistd.h>
#include <iostream>
#include "scx/PVBuffer.hpp"
#include "scx/Thread.hpp"
using namespace std;
using namespace scx;

struct Item
{
    int type;
    char data[100];
};

bool stopCustomer1 = false;
bool stopCustomer2 = false;
bool stopRouter = false;
bool stopProducer = false;

void productor(PVBuffer<Item>* buffer)
{
    cout << buffer << endl;
    cout << "begin productor" << endl;
    for (int i = 0; true; ++i)
    {
	Item* pItem = buffer->TakeFree();

	if (stopProducer)
	    break;

	pItem->type = i&1;
	*((int*)pItem->data) = 0;

	// Provide to the router.
	buffer->RecycleFree(pItem);
	cout << "." << flush;
    }

   cout << "end productor" << endl;
}

void router(PVBuffer<Item>* buffer, PVBuffer<Item>* buffer1, PVBuffer<Item>* buffer2)
{
    while (true)
    {
	Item* pItem = buffer->TakeData();

	if (stopRouter)
	    break;

	switch (pItem->type)
	{
	    case 0:
	    {
		// Distribute to customer1.
		buffer1->RecycleFree(pItem);
	    }
	    break;

	    case 1:
	    {
		// Distribute to customer2.
		buffer2->RecycleFree(pItem);
	    }
	    break;

	    default:
	    {
		// Recycle directly.
		cout << "FATAL: bad type" << pItem->type << endl;
		buffer->RecycleData(pItem);
	    }
	    break;
	}
    }

    cout << "end router" << endl;
    return;
}

void customer1(PVBuffer<Item>* buffer, PVBuffer<Item>* buffer1)
{
    //cout << buffer << endl;
    while (true)
    {
	sleep(1);
	// Take data from buffer1, recycle to buffer.
	Item* pItem = buffer1->TakeData();

	if (stopCustomer1)
	    break;

	cout << pItem->type << flush;
	
	buffer->RecycleData(pItem);
    }

    cout << "end customer1" << endl;
}

void customer2(PVBuffer<Item>* buffer, PVBuffer<Item>* buffer2)
{
    //cout << buffer << endl;
    while (true)
    {
	// Take data from buffer1, recycle to buffer.
	Item* pItem = buffer2->TakeData();

	if (stopCustomer2)
	    break;

	cout << pItem->type << flush;
	
	buffer->RecycleData(pItem);
    }

    cout << "end customer2" << endl;
}

int main()
{
    PVBuffer<Item> buffer;
    buffer.AllocBuffer(100);

    PVBuffer<Item> buffer1;
    PVBuffer<Item> buffer2;

    cout << &buffer << endl;

    // producer thread
    Thread thProducer;
    thProducer.Run(Function<void (PVBuffer<Item>*)>(&productor), &buffer);

    // router thread
    Thread thRouter;
    thRouter.Run(
	    Function<void (PVBuffer<Item>*, PVBuffer<Item>*, PVBuffer<Item>*)>(&router), 
	    &buffer, &buffer1, &buffer2);

    // customers
    Thread thc1;
    thc1.Run(Function<void (PVBuffer<Item>*, PVBuffer<Item>*)>(&customer1), &buffer, &buffer1);
    Thread thc2;
    thc2.Run(Function<void (PVBuffer<Item>*, PVBuffer<Item>*)>(&customer2), &buffer, &buffer2);

    sleep(2);

    // wait for finish
    cout << "try to stop producer " << endl;
    stopProducer = true;
    buffer.RecycleData(NULL);
    thProducer.Join();

    cout << "try to stop router " << endl;
    stopRouter = true;
    buffer.RecycleFree(NULL);
    thRouter.Join();

    sleep(5);

    cout << "try to stop customer1 " << endl;
    stopCustomer1 = true;
    buffer1.RecycleFree(NULL);
    thc1.Join();

    cout << "try to stop customer2 " << endl;
    stopCustomer2 = true;
    buffer2.RecycleFree(NULL);
    thc2.Join();

    // check buffer status
    cout << "buffer free item count:" << buffer.GetFreeCount() << endl;

    buffer.ClearBuffer();
    cout << "buffer free item count:" << buffer.GetFreeCount() << endl;

    return 0;
}
