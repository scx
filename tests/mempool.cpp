#include <iostream>
#include "scx/MemPool.hpp"
using namespace std;
using namespace scx;

int main() {

    // The BLOCK_STEP at least should be sizeof(void*).
    // It means on amd64 the min step is 8, while on i386 the min step is 4.
    MemPool<8, 16> pool;

    cout << "void* size:" << sizeof(void*) << endl;
    cout << "pool size:" << pool.Size() << endl;

    int* data = (int*)pool.Alloc(sizeof(int));
    *data = 10;
    cout << *data << endl;
    data = (int*)pool.Alloc(sizeof(int));
    *data = 99;
    cout << *data << endl;
    pool.Reuse(data, sizeof(int));

    char* buf = (char*)pool.Alloc(100);
    pool.Reuse(buf, 100);

    cout << "pool size:" << pool.Size() << endl;

    return 0;
}
