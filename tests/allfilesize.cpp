#include <iostream>
using namespace std;

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

int main(int argc, char** argv)
{
    struct stat file_stat;
    stat(argv[1], &file_stat);
    cout << file_stat.st_size << endl;

    return 0;
}
