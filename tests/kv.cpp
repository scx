#include <iostream>
#include <sstream>
#include "scx/RunTimer.hpp"
#include "scx/SqliteKV.hpp"
using namespace std;
using namespace scx;

const int COUNT_NAME = 9999;

void write(SqliteKV& db)
{
    db.Begin();
    for (int i = 0; i < COUNT_NAME; ++i)
    {
	cout << '\r' << (int)((double)(i+1)/COUNT_NAME*100) << "%";
	stringstream stream;
	stream << "name" << i;
	string key = stream.str();

	stream.str("");
	stream << "student" << i;
	string value = stream.str();

	db.PutString(key, value);
    }
    db.Commit();
    cout << endl;
}

void read(SqliteKV& db)
{
    for (int i = 0; i < COUNT_NAME; ++i)
    {
	stringstream stream;
	stream << "name" << i;
	string key = stream.str();
	string value;
	db.GetString(key, value);
	cout << value << endl;
    }
    cout << endl;
}

void update(SqliteKV& db)
{
    db.Begin();
    for (int i = 0; i < COUNT_NAME; ++i)
    {
	cout << '\r' << (int)((double)(i+1)/COUNT_NAME*100) << "%";
	stringstream stream;
	stream << "name" << i;
	string key = stream.str();

	stream.str("");
	stream << "student" << (COUNT_NAME-i-1);
	string value = stream.str();

	//db.Delete(key);
	//db.PutString(key, value);
	db.UpdateString(key, value);
    }
    db.Commit();
    cout << endl;
}

void find(SqliteKV& db, const string& key)
{
    cout << "find:" << key << endl;
    string text;
    if (db.GetString(key, text))
	cout << '\t' << text << endl;
    else
	cout << "\tnot found or null" << endl;
}

int main()
{
    SqliteKV db("kv.db");
    db.SwitchTable("hello");
    db.DropTable();
    db.SwitchTable("hello");

    write(db);
    read(db);

    cout << "done" << endl;
    cout << "count:" << db.Count() << endl;

    char ch;
    cin >> ch;

    RunTimer t;
    t.Start();
    update(db);
    t.Stop();

    read(db);

    cout << "ms:" << t.DiffMS() << endl;

    find(db, "xx");
    find(db, "name0");
    return 0;
}
