#include <iostream>
#include <vector>
#include "scx/Signal.hpp"
#include "scx/Alias.hpp"
#include "scx/RunTimer.hpp"
using namespace std;

#ifdef BOOST
#include <boost/signals.hpp>
#include <boost/bind.hpp>
#endif

/*
const size_t COUNT = 999999;

class Sender
{
    public:
	static Sender* instance()
	{
	    static Sender m_instance;
	    return &m_instance;//return m_instance != NULL ? m_instance : m_instance = new B;
	}

	scx::Signal<int (int)> sig1;
	scx::Signal<int ()> sig2;

#ifdef BOOST
	boost::signal<int (int)> sigb1;
	boost::signal<int ()> sigb2;
#endif

};

class ListenerA
{
    public:
	ListenerA()
	{
	    Sender::instance()->sig1.Connect(&ListenerA::get1, this);
	    Sender::instance()->sig2.Connect(&ListenerA::get2, this);

#ifdef BOOST
	    Sender::instance()->sigb1.connect(boost::bind(&ListenerA::get1, this, _1));
	    Sender::instance()->sigb2.connect(boost::bind(&ListenerA::get2, this));
#endif
	}

	~ListenerA()
	{
	    Sender::instance()->sig1.DisconnectReceiver(this);
	}

    private:
	int get1(int x)
	{
	    cout << "received:" << x << endl;
	    return x*x;
	}

	int get2()
	{
	    cout << "got" << endl;
	    return 0;
	}
};
*/

class Sigsender
{
public:
    scx::Signal<void (int)> sigInt;
};

class RecvA
{
public:
    void slotInt(int val)
    {
	cout << "RecvA, slotInt() val:" << val << endl;
    }
    
    void slotData(int val)
    {
	cout << "RecvA, slotData() val:" << val << endl;
    }
};

class RecvB
{
public:
    void slotInt(int val)
    {
	cout << "RecvB, slotInt() val:" << val << endl;
    }
};

int main()
{
    Sigsender sender;
    RecvA a, a1;
    RecvB b;

    sender.sigInt.Connect(&RecvB::slotInt, &b);

    sender.sigInt.Connect(&RecvA::slotInt, &a);
    sender.sigInt.Connect(&RecvA::slotData, &a);
    sender.sigInt.Connect(&RecvA::slotInt, &a1);
    sender.sigInt.Connect(&RecvA::slotData, &a1);

    cout << "Signal1" << endl;
    sender.sigInt(10);

    sender.sigInt.DisconnectReceiver(&a1);
    // for test
    //sender.sigInt.DisconnectReceiver(&b);
    //sender.sigInt.DisconnectReceiver(&a1);

    cout << "Signal2" << endl;
    sender.sigInt(20);

    /*
    ListenerA a;
    
    scx::RunTimer timer;

#ifdef BOOST
    timer.Start();
    for (size_t i = 0; i < COUNT; ++i)
    {
	B::instance()->sigb1(i);
	//B::instance()->sigb2();
    }
    timer.Stop();
    timer.PrintMS();
#endif

    timer.Start();
    for (size_t i = 0; i < COUNT; ++i)
    {
	Sender::instance()->sig1(i);
	//B::instance()->sig2();
    }
    timer.Stop();
    timer.PrintMS();

    return 0;
    */
}
