#include "scx/FileHelp.hpp"

#include <iostream>
using namespace std;
using namespace scx;

int main(int argc, char** argv)
{
    cout << FileSuffix("foo.bar.iso") << endl;

    cout << FileSize(argv[1]) << endl;
    
    if (CreateFile("test.x", 1024*1204*1024))
	cout << "successed!";
    else
	cout << "failed";
    cout << endl;

    cout << ">>> Test FileDir()" << endl;
    cout << ".\t" << FileDir(".") << endl;
    cout << "./\t" << FileDir("./") << endl;
    cout << "./foo\t" << FileDir("./foo") << endl;
    cout << "./foo/bar\t" << FileDir("./foo/bar") << endl;
    cout << "../\t" << FileDir("../") << endl;
    cout << "../foo/\t" << FileDir("../foo/") << endl;
    cout << "../foo/bar\t" << FileDir("../foo/bar") << endl;

    cout << endl;

    return 0;
}
