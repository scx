#include <iostream>
#include <stack>
#include <deque>
#include <vector>
#include "scx/Stack.hpp"
#include "scx/RunTimer.hpp"
using namespace std;
using namespace scx;

int main() {
    // usage
    {
	StaticStack<string, 100> strstack;
	strstack.Push("! ");
	strstack.Push("world ");
	strstack.Push("hello ");
	cout << strstack.At(0) << strstack.At(1) << strstack.At(2) << endl;
	cout << strstack.Pop() << strstack.Pop() << strstack.Pop() << endl;
    }

    // benchmark
    {
	const int times = 9999;
	const int loops = 9999;

	StaticStack<int, loops> mystack;
	SCX_BENCH4("my stack:\t", times, loops, mystack.Push(j), mystack.Clear());

	vector<int> stlvector;
	stlvector.reserve(loops);
	SCX_BENCH4("stl vector:\t", times, loops, stlvector.push_back(j), stlvector.clear());

	vector<int> stldeque;
	SCX_BENCH4("stl deque:\t", times, loops, stldeque.push_back(j), stldeque.clear());

	stack<int> stlstack;
	SCX_BENCH4("stl stack:\t", times, loops, stlstack.push(j), 
		for (int i = 0; i < loops; ++i) {
		    stlstack.pop();
		});
    }

    return 0;
}
