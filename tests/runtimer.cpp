#include "scx/RunTimer.hpp"
#include "scx/Function.hpp"

#ifdef BOOST
#include <boost/bind.hpp>
#include <boost/function.hpp>
#endif

#ifdef TR1
#include <tr1/functional>
#include <tr1/memory>
#endif

using namespace std;

const int TIMES = 99999999;
int fun(int a, int b)
{
   return a+b; 
}

class Add
{
public:
    Add(): sum(0) {

    }

    int fun(int a, int b)
    {
	int c = a+b;
	sum += c;
	return c;
    }

    void setSum(double sum) {
	this->sum = sum;
    }

    double getSum() {
	return sum;
    }

private:
    double sum;
};

int main(int argc, char** argv)
{
    Add add1;

    typedef int (*pfn1_t)(int, int);
    pfn1_t f1 = &fun;
    
    typedef int (Add::*pfn2_t)(int, int);
    pfn2_t f2 = &Add::fun;
   
    scx::FunctionMem<int (Add*, int, int)>* f3 = new scx::FunctionMem<int (Add*, int, int)>(&Add::fun, &add1);
    scx::FunctionMem<int (Add*, int, int)> f3_1(&Add::fun, &add1);

    scx::Function<int (int, int)> f4(&Add::fun, &add1);

#ifdef BOOST
    boost::function<int (int, int)> f5 = boost::bind(&Add::fun, &add1, _1, _2);
#endif

#ifdef TR1
    tr1::function<int (int, int)> f6 = tr1::bind(&Add::fun, &add1, tr1::placeholders::_1, tr1::placeholders::_2);
#endif

    int times = 50;
    int loops = 9999999;
    SCX_BENCH14("c ptr:\t", times, loops,
	    sum += f1(j, j/2),
	    double sum = 0,
	    sum = 0;);

    SCX_BENCH0("mem ptr:\t", times, loops,
	    (&add1->*f2)(j, j/2));

    SCX_BENCH0("scx pMem:\t", times, loops,
	    (*f3)(j, j/2));

    SCX_BENCH0("scx Mem:\t", times, loops,
	    f3_1(j, j/2));

    SCX_BENCH0("scx Function:\t", times, loops,
	    f4(j, j/2));

#ifdef BOOST
    SCX_BENCH0("boost Function:\t", times, loops,
	    f5(j, j/2));
#endif

#ifdef TR1
    SCX_BENCH0("tr1 Function:\t", times, loops,
	    f6(j, j/2));
#endif

}
