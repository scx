#include "scx/Thread.hpp"
#include <iostream> 
using namespace std;
using namespace scx;

void hello(int a, int b)
{
    cout << a*b << endl;

}

class test
{
    public:
	test(char ch):
	    mCh(ch)
	{
	}

	void print(int a)
	{
	    for (size_t i = 0; i < 100; ++i)
	    {
		cout << mCh;
	    }
	    cout << endl << a << endl;
	}

    private:
	char mCh;
};

int main()
{
    test a('a');
    test b('b');
    test c('c');
    Function<void (int)> f1(&test::print, &a);
    Function<void (int)> f2(&test::print, &b);
    Function<void (int)> f3(&test::print, &c);

    Thread th1(f1, 1);    
    Thread th2(f2, 2);
    Thread* th3 = new Thread();
    th3->Run(f3, 3);

    cout << th1.GetId() << endl;
    cout << th2.GetId() << endl;
    cout << th3->GetId() << endl;

    th1.Join();
    th2.Join();
    th3->Join();
    delete th3;
    
    
    //Function<void (int, int)> f4(&hello);
    Thread th4;
    th4.Run(Function<void (int, int)>(&hello), 4, 5);
    th4.Detach();    
    sleep(2);

    cout << endl;

    return 0;
}
