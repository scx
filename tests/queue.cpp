#include <iostream>
#include <deque>
#include <queue>
#include "scx/Queue.hpp"
#include "scx/RunTimer.hpp"
using namespace std;
using namespace scx;

int main() {
    {
	Queue<string, 2> myQueue;
	for (int i = 0; i < 10; ++i) {
	    char ch = i+65;
	    myQueue.PushBack(string(&ch, 1));
	}
	for (int i = 0; i < 10; ++i) {
	    cout << myQueue.PopFront() << ",";
	}
	cout << endl;
    }

    const int times = 50;
    const int loops = 9999999;

    queue<double> stlQueue;
    deque<double> stlDeque;
    Queue<double, 10> myQueue;
    //myQueue.SetCacheSize(count);
    //queue2.SetCacheSize(count);

    SCX_BENCH4("my Queue\t", times, loops,
	    myQueue.PushBack(i), myQueue.Clear());

    SCX_BENCH4("stl Deque\t", times, loops,
	    stlDeque.push_back(i), stlDeque.clear());

    SCX_BENCH4("my Queue\t", times, loops*1.2,
	    myQueue.PushBack(i), myQueue.Clear());

    SCX_BENCH4("stl Deque\t", times, loops,
	    stlDeque.push_back(i), stlDeque.clear());

    SCX_BENCH4("stl Queue\t", times, loops,
	    stlQueue.push(i), 
	    for (int i = 0; i < loops; ++i) {
		stlQueue.pop();
	    });

    return 0;
}
