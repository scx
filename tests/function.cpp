#include <iostream>
#include <vector>
#include <algorithm>
#include "scx/Function.hpp"
#include "scx/Alias.hpp"
using namespace std;
using namespace scx;

struct A {
    int funintint(int x) {
	return x*x;
    }
};

struct B {
    int fun2int(int x, int y, int z, int a, int b) {
	return x*y*z*a*b;
    }
};

void funvoidint(int x) {
    cout << x*x*x << ", ";
}

int main() {
    vector<int> list;
    list.resize(10);
    foreach_linear(i, list) {
	list[i] = i;
    }

    Function<void (int)> f(&funvoidint);
    for_each(list.begin(), list.end(), f);
    cout << endl;

    B b;
    Function<int (int, int, int, int, int)> f2(&B::fun2int, &b);
    cout << f2(1, 2, 3, 4, 5) << endl;

    return 0;
}

