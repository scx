#include <iostream>
#include "scx/Socket.hpp"
#include "scx/Thread.hpp"
using namespace std;
using namespace scx;

/**
 * cli send tcp msg to tcp server
 * tcp server receive the msg, send udp msg to udp server
 * udp server get the msg, print it out
 */

void udpListen() {
    UdpSocket sock;
    SocketOpt opt;
    sock.SetOption(opt);

    cout << "udp server fd:" << sock.GetFd() << endl;
    cout << "udp server bind:" << sock.Bind(1987) << endl;

    char buffer[1024];
    while (true) {
	InetAddr clientAddr;
	ssize_t n = sock.RecvFrom(buffer, sizeof(buffer), clientAddr);
	cout << "udp server client ip:" << clientAddr.GetIp() << endl;
	cout << "udp server client port:" << clientAddr.GetPort() << endl;

	string str;
	if (n > 0) {
	    str.assign(buffer, n);
	    cout << "udp server got:" << str << endl;
	} else  {
	    cout << "udp server recv:" << n << endl;
	}

	if (str == "quit")
	    break;
    }
}

void tcpListen() {
    TcpSocket sock;
    SocketOpt opt;
    sock.SetOption(opt);

    cout << "tcp server fd:" << sock.GetFd() << endl;
    cout << "tcp server bind:" << sock.Bind(1027) << endl;
    cout << "tcp server listen:" << sock.Listen(20) << endl;

    cout << "tcp server ip:" << sock.GetAddr().GetIp() << endl;
    TcpSocket clientSock;
    sock.Accept(clientSock);
    InetAddr clientAddr = clientSock.GetAddr();
    cout << "tcp server connfd:" << clientSock.GetFd() << endl;
    cout << "tcp server client ip:" << clientAddr.GetIp() << endl;
    cout << "tcp server client port:" << clientAddr.GetPort() << endl;
    char buffer[1024];
    while (true) {
	ssize_t n = clientSock.Recv(buffer, sizeof(buffer));
	string str;
	if (n > 0) {
	    str.assign(buffer, n);
	    cout << "tcp server got:" << str << endl;
	    InetAddr udpAddr(1987);
	    UdpSocket udpSock;
	    udpSock.SendTo(str.data(), str.size(), udpAddr);
	} else {
	    cout << "tcp server recv:" << n << endl;
	}

	if (str == "quit")
	    break;
    }
}

int main() {
    Thread udpThread;
    udpThread.Run(Function<void (void)>(&udpListen));

    Thread tcpThread;
    tcpThread.Run(Function<void (void)>(&tcpListen));

    sleep(1);

    TcpSocket sock;
    cout << "client fd:" << sock.GetFd() << endl;
    InetAddr serverAddr("127.0.0.1", 1027);
    cout << "client connect:" << sock.Connect(serverAddr) << endl;
    while (true) {
	string a;
	cin >> a;
	ssize_t n = sock.Send(a.data(), a.size());
	cout << "client send:" << n << endl;

	if (a == "quit")
	    break;
    }

    udpThread.Join();
    tcpThread.Join();

    return 0;
}
