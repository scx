#include <iostream>
#include "scx/Html.hpp"
using namespace std;
using namespace scx;

int main(int argc, char** argv)
{
    if (argc < 2)
    {
	cout << "Usage: ./html <HtmlFileName>" << endl;
	return -1;
    }

    cout << "filename:" << argv[1] << endl;

    HtmlTable table;
    vector<HtmlTableData> allData;

    table.LoadFile(argv[1]);
    HtmlTablesInfo info = table.CheckInfo();
    table.ReadAll(allData);

    cout << "tables:" << info.size() << endl;
    for (size_t i = 0; i < info.size(); ++i)
    {
	size_t rows = info[i].rows;
	size_t cols = info[i].cols;

	cout << "table" << i << endl;
	cout << "\trows:" << rows << endl;
	cout << "\tcols:" << cols << endl;

	for (size_t r = 0; r < rows; ++r)
	{
	    cout << r+1 << "\t";
	    for (size_t c = 0; c < cols; ++c)
	    {
		cout << allData[i][r][c] << "\t";
	    }
	    cout << endl;
	}
	cout << endl;
    }

    return 0;
}
