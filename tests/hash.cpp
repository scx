#include "scx/Hash.hpp"
using namespace scx;
#include <iostream>
#include <sstream>
using namespace std;

int main()
{
    for (int i = 0; i < 10; ++i)
    {
	stringstream stream;
	stream << "foo" << i;
	string key = stream.str();
	cout << Hash(key.c_str(), key.size(), 0) << endl;
    }

    return 0;
}
