#include <iostream>
#include "scx/Mutex.hpp"
#include "scx/CondVar.hpp"
#include "scx/SemVar.hpp"
using namespace std;
using namespace scx;

int main()
{
    Mutex mutex;
    mutex.Lock();
    mutex.Unlock();

    CondVar* condVar = new CondVar(mutex);
    delete condVar;

    SemVar* semVar = new SemVar(0, 0);
    cout << "sem val:" << semVar->GetValue() << endl;
    semVar->Post();
    cout << "sem val:" << semVar->GetValue() << endl;
    semVar->Wait();
    cout << "sem val:" << semVar->GetValue() << endl;
    delete semVar;

    return 0;
}
