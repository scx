#include <iostream>
#include <string>
#include "scx/Conv.hpp"
using namespace std;
using namespace scx;

int main()
{
    double a = 100.0047;
    string stra = NumToStr(a, 5); 
    cout << stra << endl;

    string strb = "3.1415";
    double b = StrToNum<double>(strb);
    cout << b << endl;

    cout << StrToNum<size_t>(strb) << endl;

    string stru("Hello, world!");
    cout << stru << endl;
    cout << ToLower(stru) << endl;
    cout << ToUpper(stru) << endl;

    return 0;
}

