#include "scx/Alias.hpp"
#include <iostream>
#include <string>
#include <vector>
using namespace std;

struct test
{
     string a;
     int b;
};
    
int main()
{
    // align
    cout << "SCX_ALIGN(0, 15), " << SCX_ALIGN(0, 15) << endl;
    cout << "SCX_ALIGN(1, 15), " << SCX_ALIGN(1, 15) << endl;
    cout << "SCX_ALIGN(15, 15), " << SCX_ALIGN(15, 15) << endl;
    cout << "SCX_ALIGN(16, 15), " << SCX_ALIGN(16, 15) << endl;
    cout << "SCX_ALIGN(30, 15), " << SCX_ALIGN(30, 15) << endl;
    cout << "SCX_ALIGN(31, 15), " << SCX_ALIGN(31, 15) << endl;
    cout << "SCX_QALIGN(1, 15), " << SCX_QALIGN(1, 15) << endl;
    cout << "SCX_QALIGN(20, 15), " << SCX_QALIGN(20, 15) << endl;
    cout << endl;
    cout << "SCX_ALIGN(0, 16), " << SCX_ALIGN(0, 16) << endl;
    cout << "SCX_ALIGN(16, 16), " << SCX_ALIGN(16, 16) << endl;
    cout << "SCX_QALIGN(0, 16), " << SCX_QALIGN(0, 16) << endl;
    cout << "SCX_QALIGN(15, 16), " << SCX_QALIGN(15, 16) << endl;
    cout << "SCX_QALIGN(16, 16), " << SCX_QALIGN(16, 16) << endl;
    cout << "SCX_QALIGN(17, 16), " << SCX_QALIGN(17, 16) << endl;
    cout << "SCX_QALIGN(33, 16), " << SCX_QALIGN(33, 16) << endl;

    // foreach
    vector<int> list;
    list.resize(10);

    foreach_linear(i, list)
    {
	list[i] = i;
    }
    
    cout << "foreach_linear:";
    foreach_linear(i, list)
    {
	cout << list[i] << ", ";
    }
    cout << endl;

    cout << "range_linear:";
    forrange_linear(i, 2, 5)
    {
	cout << list[i] << ", ";
    }
    cout << endl;

    cout << "foreach_iterator:";
    foreach_iterator(vector<int>::iterator, iter, list)
    {
	cout << *iter << ", ";
    }
    cout << endl;
    
    cout << "range_iterator:";
    forrange_iterator(vector<int>::iterator, iter, list.begin(), list.begin()+2)
    {
	cout << *iter << ", ";
    }
    cout << endl;

    return 0;
}
