#include <iostream>
#include "scx/ColStr.hpp"
using namespace std;
using namespace scx;

int main()
{
    string str("hello");
    cout << BlueStr(str);
    cout << YellowStr(" world");
    cout << RedStr("!") << endl;
}
