#include "scx/Singleton.hpp"
#include <iostream>
using namespace std;
using namespace scx;

class Conf {
public:
    Conf(): mPath("/foo") {}

    string GetPath() {
	return mPath;
    }

    void SetPath(const string& path) {
	mPath = path;
    }

private:
    string mPath;
};

int main() {
    Singleton<Conf> conf;
    cout << conf.Instance().GetPath() << endl;
    cout << &conf.Instance() << endl;

    Singleton<Conf> conf2;
    cout << &conf2.Instance() << endl;

    conf2.Instance().SetPath("/foo/bar");
    cout << conf.Instance().GetPath() << endl;

    return 0;
}
