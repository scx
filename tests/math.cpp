#include <iostream>
#include "scx/Math.hpp"
using namespace std;
using namespace scx;

void DoCheckOdd()
{
    while (true)
    {
	int num;
	cin >> num;
	cout << (IsOdd(num) ? "odd" : "even") << endl;
    }
}

void DoRoundOff()
{
    //double a = 90.1;
    //double delta = 0.00001;

    for (size_t i = 0; i < 99999; ++i)
    {
	
    }
    while (true)
    {
	double num;
	size_t off;
	cin >> num;
	cin >> off;
	cout << RoundOff(num, off) << endl;
    }
}

void DoCheck2Exp()
{
    for (int i = 0; i < 100; ++i)
    {
	if (Is2Exp(i))
	    cout << i << ", ";
    }
    cout << endl;
}

int main()
{
    cout << "Odd(o)" << endl;
    cout << "RoundOff(r)" << endl;
    cout << "2Exp(2)" << endl;

    char ch;
    cin >> ch;
    switch (ch)
    {
	case 'o':
	    DoCheckOdd();
	    break;

	case 'r':
	    DoRoundOff();
	    break;

	case '2':
	    DoCheck2Exp();
	    break;
    }
}
